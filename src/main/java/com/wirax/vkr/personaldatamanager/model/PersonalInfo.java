package com.wirax.vkr.personaldatamanager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@Table(name = "personal_info")
@NoArgsConstructor
@AllArgsConstructor
public class PersonalInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private UUID userId;
    private UUID technicalManager;
    private String characteristic;
    @ElementCollection
    private Set<UUID> successfulTrainings;
    @ElementCollection
    private Set<UUID> failedTrainings;
}
