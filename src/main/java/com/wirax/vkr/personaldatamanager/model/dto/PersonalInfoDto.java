package com.wirax.vkr.personaldatamanager.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonalInfoDto {
    private UUID id;
    private UUID user;
    private UUID technicalManager;
    private String characteristic;
    private Set<UUID> successfulTrainings;
    private Set<UUID> failedTrainings;
}
