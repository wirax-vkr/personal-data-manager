package com.wirax.vkr.personaldatamanager.controller;

import com.wirax.vkr.personaldatamanager.mapper.PersonalInfoMapper;
import com.wirax.vkr.personaldatamanager.mapper.PersonalInfoMapperImpl;
import com.wirax.vkr.personaldatamanager.model.PersonalInfo;
import com.wirax.vkr.personaldatamanager.model.dto.PersonalInfoDto;
import com.wirax.vkr.personaldatamanager.service.PersonalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController("/user")
public class PersonalInfoController {
    public final PersonalInfoService service;
    private PersonalInfoMapper mapper = new PersonalInfoMapperImpl();

    @Autowired
    public PersonalInfoController(PersonalInfoService service) {
        this.service = service;
    }

    @PostMapping
    public PersonalInfoDto create(@RequestBody PersonalInfoDto user) {
        return mapper.toDTO(service.create(mapper.toEntity(user)));
    }

    @PutMapping
    public PersonalInfoDto update(@RequestParam UUID id, @RequestBody PersonalInfoDto user) {
        return mapper.toDTO(service.update(id, mapper.toEntity(user)));
    }

    @GetMapping
    public List<PersonalInfoDto> findAll() {
        return service.findAll()
                .stream()
                .map(mapper::toDTO)
                .collect(Collectors.toList());
    }

    @DeleteMapping
    public void delete(@RequestParam UUID id) {
        service.delete(id);
    }
}
