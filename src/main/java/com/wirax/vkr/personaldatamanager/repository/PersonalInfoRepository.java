package com.wirax.vkr.personaldatamanager.repository;

import com.wirax.vkr.personaldatamanager.model.PersonalInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PersonalInfoRepository extends JpaRepository<PersonalInfo, UUID> {
}
