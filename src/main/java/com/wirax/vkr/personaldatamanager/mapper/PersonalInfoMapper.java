package com.wirax.vkr.personaldatamanager.mapper;

import com.wirax.vkr.personaldatamanager.model.PersonalInfo;
import com.wirax.vkr.personaldatamanager.model.dto.PersonalInfoDto;
import org.mapstruct.Mapper;

@Mapper
public interface PersonalInfoMapper {
    PersonalInfo toEntity(PersonalInfoDto dto);

    PersonalInfoDto toDTO(PersonalInfo dto);
}
