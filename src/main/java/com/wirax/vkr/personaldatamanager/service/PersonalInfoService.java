package com.wirax.vkr.personaldatamanager.service;

import com.wirax.vkr.personaldatamanager.model.PersonalInfo;
import com.wirax.vkr.personaldatamanager.repository.PersonalInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PersonalInfoService {
    private final PersonalInfoRepository repository;

    @Autowired
    public PersonalInfoService(PersonalInfoRepository repository) {
        this.repository = repository;
    }

    public PersonalInfo create(PersonalInfo technicalSpecialist) {
        return repository.save(technicalSpecialist);
    }

    public PersonalInfo update(UUID id, PersonalInfo user) {
        if(id != user.getId()) {
            throw new IllegalArgumentException("Wrong parameter id");
        }
        return repository.save(user);
    }

    public List<PersonalInfo> findAll() {
        return repository.findAll();
    }

    public void delete(UUID id) {
        repository.deleteById(id);
    }
}
