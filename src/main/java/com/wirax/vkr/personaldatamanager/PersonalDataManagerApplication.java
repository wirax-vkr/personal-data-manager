package com.wirax.vkr.personaldatamanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalDataManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonalDataManagerApplication.class, args);
    }

}
